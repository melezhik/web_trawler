# SYNOPSIS

Simple wrapper for `web_trawler`


# INSTALL

    $ sparrow plg install web_trawler

# USAGE

Basic usage:

    $ sparrow plg run web_trawler --param url=$url -- <web_trawler_params>

For example:

    $ sparrow plg run web_trawler \
    --param url=http://www.ldoceonline.com/dictionary/make-out -- \
    --processes 2 \
    --whitelist '*.mp3'
    --target ~/dictionary

See parameters description at [https://gitlab.com/dlab-indecol/web_trawler](https://gitlab.com/dlab-indecol/web_trawler)

If you need some automation:

    $ sparrow project create english

    $ sparrow task add english longman-dict web_trawler

    $ sparrow task ini english/longman-dict

      ---
      args: 
        target: /home/melezhik/dictionary
        processes: 2 
        whitelist: "*.mp3"

    $ sparrow task run english/longman-dict url=http://www.ldoceonline.com/dictionary/make-out

# Author

* The author of main script is Gorm Roedder ( gormroedder_at_gmail.com )
* The plugin maintainer is [Alexey Melezhik](https://github.com/melezhik/)



